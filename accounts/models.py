from django.db import models
from django.contrib.auth.models import AbstractUser, BaseUserManager
from master.models import AtributTambahan
from master.utils import ImageField, PathForFileModel
from django.conf import settings

class AccountManager(BaseUserManager):
	def create_user(self, username, nama_lengkap, password=None):
		"""
		Creates and saves a User with the given username, nama_lengkap and password.
		"""
		if not username:
			raise ValueError('Users must have an username')
		user = self.model(
			username=username,
			nama_lengkap=nama_lengkap,
		)
		user.set_password(password)
		user.save(using=self._db)
		return user

	def create_superuser(self, username, nama_lengkap, password):
		"""
		Creates and saves a superuser with the given identity number, nama_lengkap and password.
		"""
		user = self.create_user(username,
			password=password,
			nama_lengkap=nama_lengkap
		)
		user.is_admin = True
		user.is_superuser = True
		user.is_staff = True
		user.save(using=self._db)
		return user

class Account(AbstractUser, AtributTambahan):
	nama_lengkap = models.CharField("Nama Lengkap", max_length=100, db_index=True)
	alamat = models.CharField(max_length=255, null=True, blank=True)
	telephone = models.CharField(verbose_name='Telepon', max_length=50, null=True, blank=True)
	no_rekening = models.CharField(verbose_name='Nomor Rekening', max_length=255, null=True, blank=True)
	foto = ImageField(upload_to=PathForFileModel('profiles'), max_length=255, null=True, blank=True)

	objects = AccountManager()
	USERNAME_FIELD = 'username'
	REQUIRED_FIELDS = ['nama_lengkap'] 
	group_names = []

	def __init__(self, *args, **kwargs):
		super(Account, self).__init__(*args, **kwargs)
		if self.pk:
			self.group_names = self.groups.all().values_list('name',flat=True)

	def __str__(self):
		if self.nama_lengkap:
			return self.nama_lengkap
		return self.username

	def get_foto(self):
		if self.foto:
			return settings.MEDIA_URL+str(self.foto)
		return settings.STATIC_URL+"images/no-image.jpg"

	def is_penjahit(self):
		if hasattr(self, 'penjahit'):
			return True
		else:
			return False

	def is_customer(self):
		if hasattr(self, 'customer'):
			return True
		else:
			return False

	def as_json(self):
		return dict(id=self.id, username=self.username, nama_lengkap=self.nama_lengkap, alamat=self.alamat, telephone=self.telephone, no_rekening=self.no_rekening, email=self.email, foto=self.get_foto())

	class Meta:
		ordering = ['id']
		verbose_name = 'Akun'
		verbose_name_plural = 'Akun'

class KategoriPenjahit(models.Model):
	nama_kategori = models.CharField(verbose_name="Nama Kategori", max_length=255, null=True)
	keterangan = models.CharField(verbose_name="Keterangan", max_length=255, null=True, blank=True)

	def as_json(self):
		return dict(id=self.id, nama_kategori=self.nama_kategori, keterangan=self.keterangan)

	def __str__(self):
		if self.nama_kategori:
			return self.nama_kategori
		return str(self.id)

class Penjahit(Account):
	rating = models.FloatField(verbose_name="Rating", max_length=200, null=True, blank=True, default=5)
	lt = models.CharField(verbose_name="Latitute", max_length=200, null=True, blank=True)
	lg = models.CharField(verbose_name="Longitute", max_length=200, null=True, blank=True)
	waktu_pengerjaan = models.CharField(verbose_name="Waktu Pengerjaan", null=True, blank=True, max_length=255)
	harga = models.CharField(verbose_name="Harga", null=True, blank=True, max_length=255)
	kategori_waktu_pengerjaan = models.ForeignKey("KategoriWaktuPengerjaan", verbose_name="Kategori Waktu Pengerjaan", null=True, on_delete=models.CASCADE)
	kategori_harga = models.ForeignKey("KategoriHarga", verbose_name="Kategori Harga", null=True, on_delete=models.CASCADE)
	jadi_acuan = models.BooleanField(help_text='Apakah penjahit ini menjadi data acuan ?', default=False)
	kategori_penjahit = models.ManyToManyField(KategoriPenjahit, verbose_name="Kategori Penjahit")

	def get_rating_hitung(self):
		angka = 0
		if self.rating:
			if self.rating <= 1:
				angka = 1
			elif self.rating > 1 and self.rating <= 2:
				angka = 2
			elif self.rating > 2 and self.rating <= 3:
				angka = 3
			elif self.rating > 3 and self.rating <= 4:
				angka = 4
			elif self.rating > 4 and self.rating <= 5:
				angka = 5
		return angka

	def get_harga_rp(self):
		# from master.utils import formatrupiah
		# if self.harga:
		# 	return formatrupiah(self.harga, True)
		return "Rp. 0"

	def list_kategori_penjahit(self):
		kategori_penjahit_list = self.kategori_penjahit.all()
		if kategori_penjahit_list.exists():
			return ", ".join(str(g.nama_kategori) for g in kategori_penjahit_list)
		else:
			return "-"

	def as_json(self):
		kategori_penjahit_list = None
		if self.kategori_penjahit:
			kategori_penjahit_list = [ob.as_json() for ob in self.kategori_penjahit.all()]

		kategori_waktu_pengerjaan = None
		if self.kategori_waktu_pengerjaan:
			kategori_waktu_pengerjaan = self.kategori_waktu_pengerjaan.as_json()

		kategori_harga = None
		if self.kategori_harga:
			kategori_harga = self.kategori_harga.as_json()

		return dict(id=self.id, username=self.username, nama_lengkap=self.nama_lengkap, alamat=self.alamat, telephone=self.telephone, no_rekening=self.no_rekening, email=self.email, foto=self.get_foto(), rating=self.rating, lt=self.lt, lg=self.lg, harga=self.harga, waktu_pengerjaan=self.waktu_pengerjaan, kategori_waktu_pengerjaan=kategori_waktu_pengerjaan, kategori_harga=kategori_harga, kategori_penjahit_list_text=self.list_kategori_penjahit(), kategori_penjahit_list=kategori_penjahit_list)

	class Meta:
		verbose_name = 'Penjahit'
		verbose_name_plural = 'Penjahit'

class Customer(Account):
	pass

	class Meta:
		verbose_name = 'Customer'
		verbose_name_plural = 'Customer'

class KategoriWaktuPengerjaan(models.Model):
	angka = models.IntegerField("Angka", null=True)
	nama_kategori = models.CharField("Nama Kategori", null=True, max_length=255)

	def __str__(self):
		return "%s" % self.nama_kategori

	def as_json(self):
		return dict(id=self.id, angka=self.angka, nama_kategori=self.nama_kategori)

	class Meta:
		verbose_name = 'Kategori Waktu Pengerjaan'
		verbose_name_plural = 'Kategori Waktu Pengerjaan'

class KategoriHarga(models.Model):
	angka = models.IntegerField("Angka", null=True)
	nama_kategori = models.CharField("Nama Kategori", null=True, max_length=255)

	def __str__(self):
		return "%s" % self.nama_kategori

	def as_json(self):
		return dict(id=self.id, angka=self.angka, nama_kategori=self.nama_kategori)

	class Meta:
		verbose_name = 'Kategori Harga'
		verbose_name_plural = 'Kategori Harga'
