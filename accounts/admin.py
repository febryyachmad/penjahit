from django.contrib import admin, messages
from django.contrib.auth.admin import UserAdmin
from django.http import HttpResponseRedirect
from django.utils.safestring import mark_safe
from django.urls import reverse


from .models import Account, Penjahit, Customer, KategoriWaktuPengerjaan, KategoriHarga, KategoriPenjahit
from .forms import AccountCreationForm, AccountChangeForm

from master.utils import formatrupiah

class AccountAdmin(UserAdmin):
	add_form = AccountCreationForm
	form = AccountChangeForm
	# models = Account
	list_display = ('id', 'username', 'nama_lengkap', 'email', 'is_staff', 'is_superuser', 'aksi')
	fieldsets = (
			(None, {'fields': ('username', 'email', 'password')}),
			('Personal info', {'fields': ['nama_lengkap', 'alamat', 'telephone', 'no_rekening', 'status']}),
			('Hak Akses', {'fields': ('groups', 'is_superuser', 'is_staff', 'is_active')}),
		)

	def aksi(self, obj):
		str_ = '<a href="/accounts/account/%s/password/" class="btn btn-warning"><i class="fa fa-key"></i> Ganti Password</a>' % str(obj.id)
		return mark_safe(str_)
	aksi.short_description = "Aksi"

	def get_form(self, request, obj=None, **kwargs):
		form = super(AccountAdmin, self).get_form(request, obj, **kwargs)
		form.request = request
		return form

	# def get_fieldsets(self, request, obj = None):
		# is_admin_sistem = 'Admin Sistem' in request.user.group_names
		# if request.user.is_superuser or is_admin_sistem:
		# fieldsets = [
		# 	(None, {'fields': ('username', 'email', 'password')}),
		# 	('Personal info', {'fields': ['nama_lengkap', 'alamat', 'telephone', 'no_rekening', 'status']}),
		# 	('Hak Akses', {'fields': ('groups', 'is_superuser', 'is_staff', 'is_active')}),
		# ]
		# return fieldsets
admin.site.register(Account, AccountAdmin)

class PenjahitAdmin(UserAdmin):
	add_form = AccountCreationForm
	form = AccountChangeForm
	list_display = ('id', 'username', 'nama_lengkap', 'email', 'rating', 'get_gps', 'jadi_acuan', 'aksi')
	search_fields = ('username', 'nama_lengkap')
	fieldsets = (
			(None, {'fields': ('username', 'email', 'password')}),
			('Personal info', {'fields': ['nama_lengkap', 'alamat', 'telephone', 'no_rekening', 'foto', 'status', 'lt', 'lg', 'jadi_acuan', 'kategori_penjahit', 'rating',]}),
			('Data', {'fields': ('waktu_pengerjaan', 'kategori_waktu_pengerjaan', 'harga', 'kategori_harga')}),
			('Hak Akses', {'fields': ('groups', 'is_staff', 'is_active')}),
		)

	def get_gps(self, obj):
		if obj.lt and obj.lg:
			return mark_safe('<img src="/static/admin/img/icon-yes.svg" alt="True">')
		return mark_safe('<img src="/static/admin/img/icon-no.svg" alt="False">')
	get_gps.short_description = "GPS"

	# def get_waktu_pengerjaan(self, obj):
	# 	if obj.waktu_pengerjaan:
	# 		return "%s Hari" % str(obj.waktu_pengerjaan)
	# 	return "0 Hari"
	# get_waktu_pengerjaan.short_description = "Waktu Pengerjaan"

	# def get_harga(self, obj):
	# 	if obj.harga:
	# 		return formatrupiah(obj.harga, True)
	# 	return "Rp. 0"
	# get_harga.short_description = "Harga"

	def get_fieldsets(self, request, obj = None):
		fieldsets = self.fieldsets
		if request.user.is_penjahit():
			fieldsets = (
				(None, {'fields': ('username', 'email', 'password')}),
				('Personal info', {'fields': ['nama_lengkap', 'alamat', 'telephone', 'no_rekening', 'foto', 'status', 'lt', 'lg', 'kategori_penjahit', 'rating',]}),
				('Data', {'fields': ('waktu_pengerjaan', 'kategori_waktu_pengerjaan', 'harga', 'kategori_harga')}),
			)
		return fieldsets

	def get_readonly_fields(self, request, obj=None):
		rf = ('created_at', 'updated_at', 'last_login')
		if obj:
			if request.user.is_penjahit():
				rf = ('status', 'username', 'rating')
		return rf

	def response_change(self, request, obj):
		result = super(PenjahitAdmin, self).response_change(request, obj)
		if request.user.is_penjahit():
			messages.success(request, "Berhasil mengedit profil penjahit")
			return HttpResponseRedirect('/')
		return result

	def aksi(self, obj):
		str_ = '<a href="/accounts/account/%s/password/" class="btn btn-warning"><i class="fa fa-key"></i> Ganti Password</a>' % str(obj.id)
		return mark_safe(str_)
	aksi.short_description = "Aksi"

	# def get_fieldsets(self, request, obj = None):
	# 	return fieldsets

admin.site.register(Penjahit, PenjahitAdmin)

class CustomerAdmin(UserAdmin):
	add_form = AccountCreationForm
	form = AccountChangeForm
	list_display = ('id', 'username', 'nama_lengkap', 'email')
	fieldsets = (
			(None, {'fields': ('username', 'email')}),
			('Personal info', {'fields': ['nama_lengkap', 'alamat', 'telephone', 'no_rekening', 'status']}),
			('Hak Akses', {'fields': ('is_staff', 'is_active')}),
		)

	def aksi(self, obj):
		str_ = '<a href="/accounts/account/%s/password/" class="btn btn-warning"><i class="fa fa-key"></i> Ganti Password</a>' % str(obj.id)
		return mark_safe(str_)
	aksi.short_description = "Aksi"
	
	# def get_fieldsets(self, request, obj = None):
	# 	return fieldsets

admin.site.register(Customer, CustomerAdmin)


class KategoriWaktuPengerjaanAdmin(admin.ModelAdmin):
	pass

class KategoriHargaAdmin(admin.ModelAdmin):
	pass

admin.site.register(KategoriWaktuPengerjaan, KategoriWaktuPengerjaanAdmin)
admin.site.register(KategoriHarga, KategoriHargaAdmin)
admin.site.register(KategoriPenjahit)