from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm, ReadOnlyPasswordHashField
from .models import Account

class AccountChangeForm(UserChangeForm):
	# nama_lengkap = forms.CharField(label="", widget=forms.TextInput(attrs={'placeholder': 'Nama Lengkap'}))
	status = forms.ChoiceField(choices = ((1, 'Active'), (5, 'Archive'),), label="Status Data", initial=1, widget=forms.Select())

	# class Meta:
	# 	fields = '__all__'

	def clean_email(self):
		return self.cleaned_data['email'] or None

class AccountCreationForm(UserCreationForm):
	class Meta:
		model = Account
		fields = ('username', 'email', 'nama_lengkap',)

# class AccountChangeForm(UserChangeForm):
# 	alamat = forms.CharField(label="Alamat", required=False, widget=forms.Textarea())

# 	def clean_email(self):
# 		return self.cleaned_data['email'] or None