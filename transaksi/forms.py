from django import forms
from django.contrib.admin.widgets import FilteredSelectMultiple
from .models import Ukuran

class ModelBajuForm(forms.ModelForm):
	ukuran = forms.ModelMultipleChoiceField(
		label='Ukuran', 
		queryset=Ukuran.objects.all(),
		required=False,
		widget=FilteredSelectMultiple(
			verbose_name='Ukuran',
			is_stacked=False
		)
	)

	class Meta:
		fields = '__all__'