from django.db import models
from django.conf import settings
from master.models import AtributTambahan
from master.utils import ImageField, PathForFileModel
from accounts.models import Penjahit
from master.utils import formatrupiah, get_transaksi_pesan

class Ukuran(models.Model):
	nama_ukuran = models.CharField(verbose_name="Nama Ukuran", null=True, max_length=255)
	keterangan = models.CharField(verbose_name="Keterangan", null=True, blank=True, max_length=255)

	def __str__(self):
		return self.nama_ukuran

	def as_json(self):
		return dict(id=self.id, nama_ukuran=self.nama_ukuran, keterangan=self.keterangan)

	def as_option(self):
		return "<option value='"+str(self.id)+"'>"+str(self.nama_ukuran)+"</option>"

	class Meta:
		verbose_name = 'Ukuran'
		verbose_name_plural = 'Ukuran'

class ModelBaju(models.Model):
	penjahit = models.ForeignKey(Penjahit, verbose_name="Penjahit", null=True, on_delete=models.CASCADE)
	ukuran = models.ManyToManyField(Ukuran, verbose_name="Ukuran")
	nama_model_baju = models.CharField(verbose_name="Nama Model Baju", null=True, max_length=200)
	foto = ImageField(upload_to=PathForFileModel('baju'), max_length=255, null=True, blank=True)
	harga = models.IntegerField(verbose_name="Harga", null=True, blank=True)
	keterangan = models.CharField(verbose_name="Keterangan", null=True, blank=True, max_length=255)

	def __str__(self):
		if self.nama_model_baju:
			return self.nama_model_baju
		return str(self.id)

	def list_ukuran(self):
		ukuran_list = self.ukuran.all()
		if ukuran_list.exists():
			return ", ".join(str(g.nama_ukuran) for g in ukuran_list)
		else:
			return "-"

	def as_json(self):
		harga = 0
		if self.harga:
			harga = self.harga

		ukuran_list = None
		if self.ukuran:
			ukuran_list = [ob.as_json() for ob in self.ukuran.all()]
		return dict(id=self.id, nama_model_baju=self.nama_model_baju, foto=self.get_foto(), keterangan=self.keterangan, harga_rp=self.harga_rp(), harga=harga, ukuran=self.list_ukuran(), ukuran_list=ukuran_list)

	def harga_rp(self):
		harga = "Rp. 0"
		if self.harga:
			harga = formatrupiah(self.harga, True)
		return harga
	harga_rp.short_description = "Harga"

	def get_foto(self):
		if self.foto:
			return settings.MEDIA_URL+str(self.foto)
		return settings.STATIC_URL+"images/no-image.jpg"

	class Meta:
		verbose_name = 'Model Baju'
		verbose_name_plural = 'Model Baju'

class WaktuPengerjaan(models.Model):
	hari = models.IntegerField(verbose_name="Berapa hari ?", null=True)
	keterangan = models.CharField(verbose_name="Keterangan", null=True, blank=True, max_length=255)

	def __str__(self):
		return str(self.hari)

	def as_json(self):
		return dict(id=self.id, hari=self.hari, keterangan=self.keterangan)

	def as_option(self):
		return "<option value='"+str(self.id)+"'>"+str(self.hari)+"</option>"

	class Meta:
		verbose_name = 'Waktu Pengerjaan'
		verbose_name_plural = 'Waktu Pengerjaan'

class BiayaTambahan(models.Model):
	penjahit = models.ForeignKey(Penjahit, verbose_name="Penjahit", on_delete=models.CASCADE, related_name='penjahit_biayatambahan', null=True)
	waktu_pengerjaan = models.ForeignKey(WaktuPengerjaan, null=True, verbose_name="Waktu Pengerjaan", on_delete=models.SET_NULL)
	nominal = models.IntegerField(verbose_name="Harga Tambahan", null=True, blank=True)
	keterangan = models.CharField(verbose_name="Keterangan", null=True, blank=True, max_length=255)

	def __str__(self):
		return str(self.id)

	def as_json(self):
		return dict(id=self.id, nominal=self.nominal, keterangan=self.keterangan)

	def as_option(self):
		text = ""
		if self.waktu_pengerjaan and self.nominal:
			nominal = "Rp. 0"
			if self.nominal:
				nominal = formatrupiah(self.nominal, True)
			text = str(self.waktu_pengerjaan.hari)+" + "+str(nominal)
		return "<option value='"+str(self.id)+"'>"+text+"</option>"

	class Meta:
		verbose_name = 'Biaya Tambahan'
		verbose_name_plural = 'Waktu Pengerjaan'

class Transaksi(AtributTambahan):
	nomor_transaksi = models.CharField(unique=True, verbose_name="Nomor Transaksi", max_length=200)
	customer = models.ForeignKey('accounts.Customer', verbose_name="Customer", on_delete=models.CASCADE, related_name='customer_transaksi', null=True)
	penjahit = models.ForeignKey('accounts.Penjahit', verbose_name="Penjahit", on_delete=models.CASCADE, related_name='penjahit_transaksi', null=True)
	baju = models.ForeignKey("ModelBaju", verbose_name="Model Baju", on_delete=models.SET_NULL, null=True, blank=True, related_name='modelbaju_transaksi')
	ukuran = models.ForeignKey(Ukuran, verbose_name="Ukuran Baju", on_delete=models.SET_NULL, null=True, blank=True, related_name='ukuran_transaksi')
	total_harga = models.IntegerField(verbose_name="Total Harga", null=True, blank=True)
	biaya_tambahan = models.ForeignKey(BiayaTambahan, verbose_name="Biaya Tambahan", null=True, on_delete=models.SET_NULL)
	bukti_transfer = ImageField(upload_to=PathForFileModel('bukti_transfer'), max_length=255, null=True, blank=True)
	# waktu_pengerjaan = models.ForeignKey("WaktuPengerjaan", verbose_name="Waktu Pengerjaan", on_delete=models.SET_NULL, null=True, blank=True, related_name='waktupengerjaan_transaksi')
	keterangan_rating = models.CharField(verbose_name="Keterangan Rating", null=True, blank=True, max_length=255)
	keterangan = models.CharField(verbose_name="Keterangan", null=True, blank=True, max_length=255)

	def __str__(self):
		if self.nomor_transaksi:
			return self.nomor_transaksi
		return str(self.id)

	def save(self, *args, **kwargs):
		import random
		import datetime
		datenow = datetime.datetime.now()
		angka = random.randint(100, 10000)
		tanggal = datenow.strftime("%d")+datenow.strftime("%m")+datenow.strftime("%Y")
		urutan = Transaksi.objects.all().count()+1
		self.nomor_transaksi = str(urutan)+"/"+str(angka)+"/"+tanggal
		return super(Transaksi, self).save(*args, **kwargs)

	def get_bukti_transfer(self):
		if self.bukti_transfer:
			return settings.MEDIA_URL+str(self.bukti_transfer)
		return settings.STATIC_URL+"images/no-image.jpg"

	def as_json(self):
		baju = None
		if self.baju:
			baju = self.baju.as_json()

		# biaya_tambahan = None
		# if self.biaya_tambahan:
		# 	biaya_tambahan = self.biaya_tambahan.as_json()

		customer = None
		if self.customer:
			customer = self.customer.as_json()

		penjahit = None
		if self.penjahit:
			penjahit = self.penjahit.as_json()

		biaya_tambahan = None
		waktu_pengerjaan = ""
		if self.biaya_tambahan:
			biaya_tambahan = self.biaya_tambahan.as_json()
			if self.biaya_tambahan.waktu_pengerjaan:
				waktu_pengerjaan = self.biaya_tambahan.waktu_pengerjaan.hari

		nama_baju = "-"	
		if self.baju:
			if self.baju.nama_model_baju:
				nama_baju = self.baju.nama_model_baju

		total_harga_rp = "Rp. 0"
		if self.total_harga:
			total_harga_rp = formatrupiah(self.total_harga, True)

		nama_penjahit = "-"
		if self.penjahit:
			nama_penjahit = self.penjahit.nama_lengkap

		ukuran = ""
		if self.ukuran:
			ukuran = self.ukuran.nama_ukuran

		return dict(id=self.id, nomor_transaksi=self.nomor_transaksi, baju=baju, biaya_tambahan=biaya_tambahan, customer=customer, penjahit=penjahit, nama_baju=nama_baju, total_harga_rp=total_harga_rp, nama_penjahit=nama_penjahit, ukuran=ukuran, waktu_pengerjaan=waktu_pengerjaan, tanggal_pesan=self.created_at.strftime('%d-%m-%Y %H:%M'), status=self.status, bukti_transfer=self.get_bukti_transfer(), status_pesan=get_transaksi_pesan(self), tanggal=self.created_at.strftime('%d-%m-%Y'))

	class Meta:
		# order_by = ('-id')
		# order_with_respect_to = 'id'
		ordering = ['-id']
		verbose_name = 'Transaksi'
		verbose_name_plural = 'Transaksi'

class RiwayatTransaksi(models.Model):
	transaksi = models.ForeignKey(Transaksi, null=True, on_delete=models.CASCADE)
	nama_riwayat = models.CharField(verbose_name="Nama Riwayat", max_length=200)
	tanggal_riwayat = models.DateTimeField(verbose_name="Tanggal Riwayat", null=True, blank=True, auto_now_add=True)
	keterangan = models.CharField(null=True, blank=True, max_length=200)

	def __str__(self):
		return self.nama_riwayat

	def as_json(self):
		tanggal_riwayat = None
		if self.tanggal_riwayat:
			tanggal_riwayat = self.tanggal_riwayat.strftime("%d-%m-%Y %H:%M:%S")
		keterangan = "-"
		if self.keterangan:
			keterangan = self.keterangan
		return dict(id=self.id, nama_riwayat=self.nama_riwayat, tanggal_riwayat=tanggal_riwayat, keterangan=keterangan)

	class Meta:
		verbose_name = "Riwayat"
		verbose_name_plural = "Riwayat"

class PertanyaanKuesioner(models.Model):
	pertanyaan = models.CharField(verbose_name="Pertanyaan ?", null=True, max_length=255)
	keterangan = models.CharField(null=True, blank=True, max_length=200)

	def __str__(self):
		return self.pertanyaan

	def as_json(self):
		return dict(id=self.id, pertanyaan=self.pertanyaan, keterangan=self.keterangan)

	class Meta:
		verbose_name = "Pertanyaan Kuesioner"
		verbose_name_plural = "Pertanyaan Kuesioner"

class Kuesioner(models.Model):
	transaksi = models.ForeignKey(Transaksi, null=True, on_delete=models.CASCADE)
	pertanyaan = models.ForeignKey(PertanyaanKuesioner, null=True, on_delete=models.CASCADE)
	jawaban = models.BooleanField(help_text='Apa jawaban Anda tentang pertanyaan ini ?', default=False)

	def __str__(self):
		return str(self.id)

	class Meta:
		verbose_name = "Kuesioner"
		verbose_name_plural = "Kuesioner"