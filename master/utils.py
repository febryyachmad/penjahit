from django.db import models
from django.utils.deconstruct import deconstructible
from uuid import uuid4
import os, locale

STATUS = (
	(1, 'Active'),
	(2, 'Inactive'),
	(3, 'Blocked'),
	(4, 'Submitted'),
	(5, 'Archive'),
	(6, 'Draft'), # Input Data
	(7, 'Rejected'),
	(8, 'Pesanan Diterima'), # diterima oleh penjahit
	(9, 'Terbayar'), # telah di bayar oleh customer
	(10, 'Baju Terjahit'), # baju selesai dijahit
	(11, 'Kirim'), # baju dikirim
	(12, 'Selesai'), # transaksi selesai
	(13, 'Tolak'), # transaksi ditolak
)

def get_status_color(obj):
	warna = ""
	if obj.status == 2:
		warna = ' warning'
	elif obj.status == 4:
		warna = ' danger'
	elif obj.status == 5:
		warna = ' success'
	elif obj.status == 6:
		warna = ' info'
	return warna

def get_transaksi_pesan(obj):
	pesan = ""
	if obj.status == 6:
		pesan = "Menunggu"
	elif obj.status == 8:
		pesan = "Diterima"
	elif obj.status == 9:
		pesan = "Terbayar"
	elif obj.status == 10:
		pesan = "Baju Terjahit"
	elif obj.status == 11:
		pesan = "Dikirim"
	elif obj.status == 12:
		pesan = "Selesai"
	elif obj.status == 13:
		pesan = "Ditolak"
	return pesan

@deconstructible
class PathForFileModel(object):

	def __init__(self, sub_path):
		self.path = sub_path

	def __call__(self, instance, filename):
		ext = filename.split('.')[-1]
		# set filename as random string
		filename = '{}.{}'.format(uuid4().hex, ext)
		# return the whole path to the file
		return os.path.join(self.path, filename)

class ImageField(models.ImageField):
	def save_form_data(self, instance, data):
		if data is not None: 
			file = getattr(instance, self.attname)
			if file != data:
				file.delete(save=False)
		super(ImageField, self).save_form_data(instance, data)

def rupiah_format(angka, with_prefix=False, desimal=2):
	locale.setlocale(locale.LC_NUMERIC, 'IND')
	rupiah = locale.format("%.*f", (desimal, angka), True)
	if with_prefix:
		return "Rp. {}".format(rupiah)
	return rupiah

def formatrupiah(uang, with_prefix=False):
	respon = 0

	if uang:
		i = int(uang)
		y = str(i)
		
		if len(y) <= 3 :
			# return y
			respon = y
		else :
			p = y[-3:]
			q = y[:-3]
			respon = formatrupiah(q) + '.' + p
			
		if with_prefix:
			return "Rp. {}".format(respon)
	return respon